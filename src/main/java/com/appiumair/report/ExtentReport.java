package com.appiumair.report;

import com.appiumair.util.PropertieTools;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.ViewName;

/**
 * 测试报告
 */
public class ExtentReport {
    public ExtentReport() {}
    public static ExtentReports extent = new ExtentReports();
    public static ExtentSparkReporter sparkAll = new ExtentSparkReporter(PropertieTools.getReportTempUrl() + PropertieTools.getReportName())
            .viewConfigurer()
            .viewOrder()
            .as(new ViewName[] { ViewName.DASHBOARD, ViewName.TEST })
            .apply();
    public static ExtentSparkReporter sparkFail = new ExtentSparkReporter(PropertieTools.getReportTempUrl() + "SparkFail.html")
            .filter()
            .statusFilter()
            .as(new Status[] { Status.FAIL })
            .apply();
    public static ExtentSparkReporter sparkPass = new ExtentSparkReporter(PropertieTools.getReportTempUrl() + "SparkPass.html")
            .filter()
            .statusFilter()
            .as(new Status[] { Status.PASS })
            .apply();
}

