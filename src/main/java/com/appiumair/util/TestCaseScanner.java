package com.appiumair.util;

import java.io.File;
import java.util.*;

/**
 * 测试用例扫描工具
 */
public class TestCaseScanner {

    private Map<String, String> testCaseMap = new LinkedHashMap<>();

    public Map doScan(String packageName) {

        // 将包名称转换成路径名称
        String pathName = packageName.replace(".", "/");

        File curFile = new File(System.getProperty("user.dir") + "/target/classes/" + pathName);

        // 获取文件下的所有文件
        File[] files = curFile.listFiles();

        // 准备一个存放二级目录的list
        ArrayList<String> dirNameList = new ArrayList();

        // 对所有二级目录排序
        for (File file : files) {
            String dirName = file.getName();
            dirNameList.add(dirName);
        }
        Collections.sort(dirNameList);

        // 遍历二级目录
        Iterator it = dirNameList.iterator();
        while (it.hasNext()) {
            String subDirName = (String)it.next();
            File subDir = new File( System.getProperty("user.dir") + "/target/classes/" + pathName + "/" + subDirName);
            File[] subDirFile = subDir.listFiles();

            // 对二级目录下的所有文件遍历
            ArrayList<String> subFileList = new ArrayList();
            for (File subFile : subDirFile) {
                String subFileName = subFile.getName();
                subFileName = subFileName.substring(0, subFileName.indexOf("."));
                subFileList.add(subFileName);
            }
            // 遍历完再排序
            Collections.sort(subFileList);

            // 遍历排序好的list，处理完成装进Map中
            Iterator it1 = subFileList.iterator();
            while (it1.hasNext()) {
                String subFileName = (String)it1.next();
                testCaseMap.put(subFileName, packageName + "." + subDirName + "." + subFileName);
            }
        }
        return testCaseMap;
    }
}
